import os
from typing import Optional
from django.core.exceptions import ImproperlyConfigured


class env:
    """convert env values to currect types"""
    @staticmethod
    def _get_key(key: str, default=None):
        try:
            return str(os.environ.get(key))
        except KeyError:
            if default is not None:
                return default
            else:
                raise ImproperlyConfigured(
                    f'Set the {key} environment variable')

    @staticmethod
    def bool(key: str, default: Optional[bool] = None) -> bool:
        truethy = ('true', 'yse', 'on', '1' 't', 'y')
        return bool(env._get_key(key, default).lower() in truethy)

    @staticmethod
    def int(key: str, default: Optional[int] = None) -> int:
        return int(env._get_key(key, default))

    @staticmethod
    def list(key: str, default: Optional[list[str]] = None, sep: str = ' ') -> list[str]:
        return env._get_key(key, default).split(sep)

    @staticmethod
    def str(key: str, default: Optional[str] = None) -> str:
        return env._get_key(key, default)
